
import 'package:call_log/call_log.dart';

class CallLogProvider
{

  Future<Iterable<CallLogEntry>>  fetchCallLog() async
  {
    Iterable<CallLogEntry> entries;
    entries = await CallLog.get();
    return entries;
  }



  Future<Iterable<CallLogEntry>>  fetchAllCallLogFromTo(DateTime p_from, DateTime p_to) async
  {
    Iterable<CallLogEntry> entries;
    int from = p_from.millisecondsSinceEpoch;
    int to = p_to.millisecondsSinceEpoch;
    entries = await CallLog.query(
      dateFrom: from,
      dateTo: to,
      //type: CallType.missed
    );

    return entries;
  }

  Future<Iterable<CallLogEntry>> fetchMissedCallsFromLast15sec() async
  {
    Iterable<CallLogEntry> entries;
    entries = await CallLog.query(
      dateFrom: DateTime.now().subtract(Duration(seconds: 15)).millisecondsSinceEpoch,
      dateTo: DateTime.now().millisecondsSinceEpoch,
      //type: CallType.missed,
    );
    return entries;
  }

}