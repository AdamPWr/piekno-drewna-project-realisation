import 'package:flutter/material.dart';
import 'package:call_log/call_log.dart';
import 'package:intl/intl.dart';

import 'CallLogWraper.dart';


class ContactTile extends StatefulWidget {

  CallLogEntryWraper? call;

  ContactTile({@required this.call});

  @override
  _ContactTileState createState() => _ContactTileState();
}

class _ContactTileState extends State<ContactTile> {


  @override
  Widget build(BuildContext context) {

    var size = MediaQuery.of(context).size;

    final DateFormat formatter = DateFormat('dd-MM-yy HH:mm');
    final String formatedDate = formatter.format( DateTime.fromMillisecondsSinceEpoch(widget.call!.callInfo!.timestamp!));

    return GestureDetector(
      onTap: ()
      {
        widget.call!.isEnabled = ! widget.call!.isEnabled;
        setState(() {

        });
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: widget.call!.isEnabled ? Colors.blueAccent :  Colors.brown,
        ),

        width: size.width * 0.8,
        height: size.height *0.04,
        child: Row(

          children: [
            Icon(Icons.account_circle),
            SizedBox(width: 2,),
            Text("${widget.call?.callInfo!.number}"),
            SizedBox(width: 10,),
            Text("${widget.call!.callInfo!.name}"),
            Spacer(),
            Text("${formatedDate}", style: TextStyle(color: Colors.grey[800]),)
          ],
        ),
      ),
    );
  }
}
