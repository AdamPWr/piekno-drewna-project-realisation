import 'package:get/get.dart';
import 'package:call_log/call_log.dart';

import 'CallLogWraper.dart';


class CallLogWraperControler extends GetxController
{
  // var usr = User().obs;
  var callLogList = <CallLogEntryWraper>[].obs;

  void setCallLog(Iterable<CallLogEntry> p_callLog)
  {

    callLogList.value = p_callLog.map((e) => CallLogEntryWraper(callInfo: e, isEnabled: false)).toList();
    //callLog.assignAll(p_callLog);
    update();
    // user.u
  }

}