import 'package:pieknodrewna/common/CallLogWraper.dart';
import 'package:telephony/telephony.dart';

class SmsSender
{

  final Telephony telephony = Telephony.instance;

  Future<void> handlePermissions() async
  {
    var permissionsGranted = await telephony.requestSmsPermissions;
  }

  void sendSmsToRecipments(String message, List<CallLogEntryWraper> recipments ) async
  {
    await handlePermissions();

    for(var recipment in recipments)
      {
        telephony.sendSms(
            to: recipment.callInfo!.number!!,
            message: message
        );
      }

  }

}