import 'package:get/get.dart';
import 'package:pieknodrewna/DataBase/DBHelper.dart';
import 'package:pieknodrewna/DataBase/MessagePatternModel.dart';

class MessagePatternsController extends GetxController
{

  var _patternsModel = <MessagePatternModel>[].obs;
  DbHelper dbHelper = DbHelper();
  var selectedReplyPattern = 0
      .obs;


  void fetchMessagePatternsFromDB() async
  {
    await dbHelper.openDb();
    _patternsModel.value = await dbHelper.getReplyPatternsFromDB();
    print("Fetched ${_patternsModel.length} patterns");
    await dbHelper.close();
    //update();
  }

  List<MessagePatternModel> getMessagePatterns() => _patternsModel;

  void addMessagePattern(String message) async
  {
    var model = MessagePatternModel(message: message, id: _patternsModel.value.length);
    _patternsModel.add(model);
    await dbHelper.openDb();
    await dbHelper.insertMessagePattern(model);
    await dbHelper.close();
  }

  void deleteMeessagePattern(String message) async
  {

    // TODO zaimplementowac sytuacje jak beda dwa patterny o takiej samej zawartosci ale roznym id
    await dbHelper.openDb();
    var elementOnList = _patternsModel.value.firstWhere((element) => element.message==message);
    await dbHelper.deleteReplyPatternByID(elementOnList.id);
    _patternsModel.remove(elementOnList);
    await dbHelper.openDb();
  }

  int indexOfReplyPattern(String pattern)
  {
    return _patternsModel.value.indexWhere((element) => element.message == pattern);
  }

  void setIndexOfSelectedPattern(int index)
  {
    selectedReplyPattern.value = index;
    print(index);
  }

   getIndexOfSelectedPattern() => selectedReplyPattern;

  String getSelectedPattern() => _patternsModel[selectedReplyPattern.value].message;


}