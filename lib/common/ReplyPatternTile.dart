import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pieknodrewna/common/MessagePatternsController.dart';

class ReplyPatternTile extends StatelessWidget {

  String pattern = "";

  ReplyPatternTile({required this.pattern});


  @override
  Widget build(BuildContext context) {

    var width = MediaQuery.of(context).size.width;
    return Container(
      //height: 10,
      decoration: BoxDecoration(
        color: Colors.brown[700],
        borderRadius: BorderRadius.all(Radius.circular(5))
      ),
      child: Row(

        children: [
          Text(pattern),
          Spacer(),
          GestureDetector(
              child: Icon(Icons.delete),
            onTap: () async
            {
              var controler = Get.find<MessagePatternsController>();
              controler.deleteMeessagePattern(pattern);
            },
          )
        ],
      ),
    );
  }
}
// --------------------------------------------------------------------------------------------------------------

class ReplyPatternTileBlank extends StatelessWidget {

  String pattern = "";
  int index = 0;

  ReplyPatternTileBlank({required this.pattern, required this.index});

  final colors = [Colors.brown[700], Colors.blueAccent];

  @override
  Widget build(BuildContext context) {
    return Obx((){

      return       GestureDetector(

        behavior: HitTestBehavior.opaque,

        onTap: (){

          var controler = Get.find<MessagePatternsController>();
          controler.setIndexOfSelectedPattern(index);

        },
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 5),
          margin: EdgeInsets.symmetric(horizontal: 5),
          //height: 10,
          constraints: BoxConstraints(
              minHeight: 40
          ),
          decoration: BoxDecoration(
              color: Get.find<MessagePatternsController>().getIndexOfSelectedPattern().value == index ? colors[1] : colors[0],
              borderRadius: BorderRadius.all(Radius.circular(5))
          ),
          child: GestureDetector(child: Text(pattern, style: TextStyle(fontSize: 25),)),
        ),
      );

    }

    );
  }
}

