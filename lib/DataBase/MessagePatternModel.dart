

class MessagePatternModel
{

  MessagePatternModel({required this.id, required this.message});


  int id = 0;
  String message = "";

  Map<String, dynamic> toMap() {
    return {
      'id' : id,
      'message' : message
    };
  }
}