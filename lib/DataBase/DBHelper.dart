import 'package:pieknodrewna/common/TextConstants.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import 'MessagePatternModel.dart';

class DbHelper
{

  String db_name = "messagesDB.db";
  Database? dataBase;
  final String messageTableName = "t_messagesForPieknoApp";



  Future<void> openDb() async
  {


    Future<Database> database =  openDatabase(
        join(await getDatabasesPath() , "$db_name"),
        onCreate: (db, ver){
          db.execute(" CREATE TABLE $messageTableName(id INTEGER PRIMARY KEY ,message TEXT)");
          db.execute(" CREATE TABLE $messageTableName(number TEXT PRIMARY KEY, name TEXT, isBlackListed INTEGER)");

          // some init values
          MessagePatternModel msg1 = new MessagePatternModel(id: 0, message: "$callBack");
          MessagePatternModel msg2 = new MessagePatternModel(id: 1, message: "$email");


          db.insert(messageTableName, msg1.toMap());
          db.insert(messageTableName, msg2.toMap());
        },
        version: 1
    );
    dataBase = await database;

  }

  Future<void> close() async
  {
    await dataBase?.close();
  }

  Future<void> insertMessagePattern(MessagePatternModel messagePatternModel) async
  {
    if(!dataBase!.isOpen)
    {
      throw "You' ve forgot to open database";
    }

    await dataBase!.insert(messageTableName, messagePatternModel.toMap());
  }

  Future<void> deleteReplyPattern(MessagePatternModel messagePatternModel) async{

    if(!dataBase!.isOpen)
    {
      throw "You' ve forgot to open database";
    }

    await dataBase?.delete(messageTableName,where: 'id = ?' ,whereArgs: [messagePatternModel.id]);
  }

  Future<void> deleteReplyPatternByID(int id) async{

    if(!dataBase!.isOpen)
    {
      throw "You' ve forgot to open database";
    }

    await dataBase?.delete(messageTableName,where: 'id = ?' ,whereArgs: [id]);
  }

  Future<List<MessagePatternModel>> getReplyPatternsFromDB() async
  {
    final List<Map<String, dynamic>> maps = await dataBase!.query('$messageTableName');

    print("Mapa");
    print(maps);

    return List.generate(maps.length, (i) {
      return MessagePatternModel(id: maps[i]['id'], message: maps[i]['message']);
    });
  }

}