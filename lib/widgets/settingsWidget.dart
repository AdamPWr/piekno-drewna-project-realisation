import 'package:flutter/material.dart';
import 'package:pieknodrewna/common/MessagePatternsController.dart';
import 'package:pieknodrewna/decorations/decorations.dart';
import 'package:get/get.dart';
import 'SettingsBottomWidget.dart';
import 'SettingsTopWidget.dart';

class SettingsWidget extends StatefulWidget {
  @override
  _SettingsWidgetState createState() => _SettingsWidgetState();
}

class _SettingsWidgetState extends State<SettingsWidget> {

  final double paddingVal = 10;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
        decoration: backgroundDecoration,
        child: Column(
          children: [
            SettingsTopWidget(paddingVal: paddingVal),
            SettingsBottomWidget(size: size, paddingVal: paddingVal)
          ],
        ),
      ),
//      floatingActionButton: FloatingActionButton(
//        onPressed: (){
//          var controler = Get.find<MessagePatternsController>();
//          controler.addMessagePattern("dummy");
//        },
//      ),
    );
  }
}

