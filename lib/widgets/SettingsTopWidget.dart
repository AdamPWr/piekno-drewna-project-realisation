import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pieknodrewna/DataBase/MessagePatternModel.dart';
import 'package:pieknodrewna/common/MessagePatternsController.dart';
import 'package:pieknodrewna/common/ReplyPatternTile.dart';


class SettingsTopWidget extends StatefulWidget {
  const SettingsTopWidget({
    Key? key,
    required this.paddingVal,
  }) : super(key: key);

  final double paddingVal;

  @override
  _SettingsTopWidgetState createState() => _SettingsTopWidgetState();
}

class _SettingsTopWidgetState extends State<SettingsTopWidget> {




  Future? replyPatternsFetched;
  List<MessagePatternModel>? replyPatternsList;


  Future getReplyPatterns()async
  {
    replyPatternsList = Get.find<MessagePatternsController>().getMessagePatterns();
  }

  @override
  void initState() {
    super.initState();
    replyPatternsFetched =  getReplyPatterns();

  }


  @override
  Widget build(BuildContext context) {

    var size = MediaQuery.of(context).size;


    return Expanded(
        child: Padding(
            padding: EdgeInsets.symmetric(horizontal: widget.paddingVal, vertical: widget.paddingVal*3),
            child: Container(
              decoration: BoxDecoration(
                //color: Colors.yellowAccent,
                  border: Border.all(width: 1)),
              child: FutureBuilder(

                future: replyPatternsFetched,
                builder: (context, snapshot){
                  print("BUILDER fired");

                  if(snapshot.connectionState == ConnectionState.done)
                    {
                      print("Settings list builder has data");
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: Obx(
                            (){

                              return ListView.separated(
                                  itemBuilder: (ctx, index){
                                    return ReplyPatternTile(pattern: replyPatternsList![index].message);
                                  },
                                  separatorBuilder: (_,idx){ return SizedBox(height: 5,);},
                                  itemCount: replyPatternsList!.length);
                            }
                        ),
                      );
                    }
                  else{
                    print("Settings list builder hasn't data");
                    return Container();
                  }
                },

              ),
            )));
  }

}
