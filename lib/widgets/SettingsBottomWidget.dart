import 'package:flutter/material.dart';
import 'package:nice_button/nice_button.dart';
import 'package:get/get.dart';
import 'package:pieknodrewna/common/MessagePatternsController.dart';


class SettingsBottomWidget extends StatefulWidget {
  const SettingsBottomWidget({
    Key? key,
    required this.size,
    required this.paddingVal,
  }) : super(key: key);

  final Size size;
  final double paddingVal;

  @override
  _SettingsBottomWidgetState createState() => _SettingsBottomWidgetState();
}

class _SettingsBottomWidgetState extends State<SettingsBottomWidget> {

  TextEditingController _controler = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFormField(
                controller: _controler,
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  decoration: InputDecoration(
                    labelText: "Wprowadz nowy wzor odpowiedzi",
                    //focusColor: greenBlueColor,
                      contentPadding: EdgeInsets.symmetric(
                          vertical: widget.size.height * 0.05, horizontal: widget.paddingVal),
                      fillColor: Colors.grey,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0)))),
              SizedBox(height: 10,),
              NiceButton(
                radius: 40,
                padding: const EdgeInsets.all(15),
                text: "Zapisz",
                textColor: Colors.orange,
                //icon: Icons.add,
                gradientColors: [Colors.brown, Colors.blueGrey],
                onPressed: () {

                  if(_controler.text.length > 0)
                    {
                      var patternControler = Get.find<MessagePatternsController>();
                      patternControler.addMessagePattern(_controler.text);
                    }
                  _controler.text = "";
                }, background: null,
              )
            ],
          ),
        ));
  }
}

