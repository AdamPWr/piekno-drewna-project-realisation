import 'package:flutter/material.dart';
import 'package:pieknodrewna/widgets/selectPatternWidget.dart';

import 'inputTestMessageWidget.dart';

class HomeTopWidget extends StatefulWidget {
  @override
  _HomeTopWidgetState createState() => _HomeTopWidgetState();
}

class _HomeTopWidgetState extends State<HomeTopWidget> {

  bool sendTypedInsteadSavedMessageSwitch = false;


  TextEditingController _controler = new TextEditingController();
  @override
  Widget build(BuildContext context) {

    var size = MediaQuery.of(context).size;


    return Column(
      children: [
        Expanded(
          flex: 4,
            child: Container(
              child: Column(

                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  !sendTypedInsteadSavedMessageSwitch ? InputTestMessageWidget(controler: _controler, size: size) : SelectPatternWidget(),
                  Row(

                    children: [
                      Spacer(flex: 1,),
                      Text("Wprowadz wiadomosc / wybierz wzor"),
                      Spacer(flex: 3,),
                      Switch(
                          value: sendTypedInsteadSavedMessageSwitch,
                          activeColor: Colors.blueGrey[700],
                          onChanged: (val){
                            setState(() {
                              sendTypedInsteadSavedMessageSwitch = val;
                            });
                          }
                      ),
                      Spacer(flex: 1,),

                    ],
                  ),

                ],
              ),
            )
        ),
        Expanded(child: Container(), flex: 2,) // add baner part
      ],
    );
  }
}
