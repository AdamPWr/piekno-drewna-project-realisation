import 'package:flutter/material.dart';
import 'package:pieknodrewna/common/TextConstants.dart';

class InputTestMessageWidget extends StatelessWidget {
  const InputTestMessageWidget({
    Key? key,
    required TextEditingController controler,
    required this.size,
  }) : _controler = controler, super(key: key);

  final TextEditingController _controler;
  final Size size;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: TextFormField(
          controller: _controler,
          keyboardType: TextInputType.multiline,
          maxLines: null,
          decoration: InputDecoration(
              labelText: enterTxtMsgHint,
              //focusColor: greenBlueColor,
              contentPadding: EdgeInsets.symmetric(vertical: size.height * 0.05, horizontal: 10),
              fillColor: Colors.grey,
              border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))
          )),
    );
  }
}
