import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:pieknodrewna/widgets/homeWidget.dart';
import 'package:pieknodrewna/widgets/settingsWidget.dart';

class Root extends StatefulWidget {
  @override
  _RootState createState() => _RootState();
}

class _RootState extends State<Root> {


  var currentScreenIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: IndexedStack(

        index: currentScreenIndex,

        children: [
          HomeWidget(),
          SettingsWidget(),
        ],
      ),


      bottomNavigationBar: CurvedNavigationBar(
        buttonBackgroundColor: Colors.blueGrey,
        backgroundColor: Colors.brown[400],
        color: Colors.brown[700],
        onTap: (idx){
          setState(() {
            currentScreenIndex = idx;
          });
        },

        items: [
          Column(
            children: [
              Icon(Icons.home, size: 30, color: Colors.orange,),
              //Text("Home")
            ],
          ),
          Column(
            children: [
              Icon(Icons.settings, size: 30, color: Colors.orange,),
              //Text("Settings")
            ],
          ),


        ],
      ),
    );
  }
}
