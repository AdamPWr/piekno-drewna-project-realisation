import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pieknodrewna/DataBase/MessagePatternModel.dart';
import 'package:pieknodrewna/common/MessagePatternsController.dart';
import 'package:pieknodrewna/common/ReplyPatternTile.dart';

class SelectPatternWidget extends StatefulWidget {
  @override
  _SelectPatternWidgetState createState() => _SelectPatternWidgetState();
}

class _SelectPatternWidgetState extends State<SelectPatternWidget> {


  Future? replyPatternsFetched;
  List<MessagePatternModel>? replyPatternsList;


  Future getReplyPatterns() async
  {
    replyPatternsList =
        Get.find<MessagePatternsController>().getMessagePatterns();
  }


  @override
  void initState() {
    replyPatternsFetched = getReplyPatterns();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(child: Container(

      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5))
      ),

      child: FutureBuilder(
        future: replyPatternsFetched,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done)
            {
              return Obx(
                      () {
                    return ListView.separated(
                        itemBuilder: (ctx, index) {
                          return ReplyPatternTileBlank(
                              pattern: replyPatternsList![index].message, index: index,);
                        },
                        separatorBuilder: (_, idx) {
                          return SizedBox(height: 5,);
                        },
                        itemCount: replyPatternsList!.length);
                  }
              );
            }
          else return Container();

        },
      ),
    ));
  }
}
