import 'package:flutter/material.dart';
import 'package:pieknodrewna/PhoneHelpers/CallLog.dart';
import 'package:call_log/call_log.dart';
import 'package:pieknodrewna/common/CallLogWraper.dart';
import 'package:pieknodrewna/common/CallLogWraperControler.dart';
import 'package:pieknodrewna/common/MessagePatternsController.dart';
import 'package:pieknodrewna/common/SMSSender.dart';
import 'package:pieknodrewna/common/contactTile.dart';
import 'package:pieknodrewna/decorations/decorations.dart';
import 'package:pieknodrewna/widgets/homeTopWidget.dart';
import 'package:get/get.dart';

import 'inputTestMessageWidget.dart';

class HomeWidget extends StatefulWidget {
  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {

  Future? callLogFetched;
  Iterable<CallLogEntry> callLog = [];



  Future getCallLog() async
  {
    CallLogProvider provider = CallLogProvider();
    callLog = await provider.fetchAllCallLogFromTo( DateTime.now().subtract(Duration(days: 7)), DateTime.now());
    final callLogWraperControler = Get.put(CallLogWraperControler());
    callLogWraperControler.setCallLog(callLog);
    return;
  }


  @override
  void initState() {
    callLogFetched = getCallLog();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {



    return Scaffold(

      body: Container(

        decoration: backgroundDecoration,
        child: Column(

          children: [
            Expanded(child: SafeArea(
              child: HomeTopWidget(),
            ),),
            Expanded(
                child: FutureBuilder(
                  future: callLogFetched,
                  builder: (context,snapshot){

                    print(snapshot.connectionState.toString());
                    if(snapshot.connectionState == ConnectionState.done)
                      {
                        print("Has data ${callLog.length}");
                        return GetBuilder<CallLogWraperControler>(
                          builder: (_){
                            return ListView.separated(
                                itemBuilder: (context,index){
                                  print("Length ${_.callLogList.value.length}");
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 20),
                                    child: ContactTile(call: _.callLogList.value.elementAt(index)),
                                  );
                                },
                                separatorBuilder: (ctx,idx){
                                  return SizedBox(height: 1,);
                                },
                                itemCount: callLog.length
                            );
                          },
                         // child:
                        );
                      }
                    else
                      {
                        print("Hasnt data ${callLog.length}");
                        return Container();
                      }
                  },
                )
            )
          ],
        ),
      ),


      floatingActionButton: SizedBox(
        height: 70,
        width: 70,
        child: FloatingActionButton(
          backgroundColor: Colors.blueGrey[700],
          child: Icon(Icons.mail, color: Colors.brown[800],size:40,),
          mini: false,
          onPressed: (){
            String message = Get.find<MessagePatternsController>().getSelectedPattern();
            print(message);
            var recipments = Get.find<CallLogWraperControler>().callLogList.value.where((element) => element.isEnabled).toList();
            SmsSender sender = SmsSender();
            sender.sendSmsToRecipments(message, recipments);

            final snackBar = SnackBar(content: Text('Wyslano wiadomosci'));
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          },
        ),
      ),
    );
  }
}

