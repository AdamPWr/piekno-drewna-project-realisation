import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pieknodrewna/common/MessagePatternsController.dart';

import 'widgets/root.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  var messagePatterns = Get.put(MessagePatternsController(),permanent: true);
  messagePatterns.fetchMessagePatternsFromDB();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Piekno drewna',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: Root()//HomeWidget()
    );
  }
}

